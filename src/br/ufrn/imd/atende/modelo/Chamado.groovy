package br.ufrn.imd.atende.modelo

class Chamado {
	int id
	Usuario responsavel
	String assunto
	String descricao
	String sala
	String ramal
	List<MovimentacaoChamado> movimentacoes
}
