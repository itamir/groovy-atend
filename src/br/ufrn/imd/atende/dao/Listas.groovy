package br.ufrn.imd.atende.dao

import br.ufrn.imd.atende.modelo.Chamado
import br.ufrn.imd.atende.modelo.MovimentacaoChamado
import br.ufrn.imd.atende.modelo.SituacaoChamado
import br.ufrn.imd.atende.modelo.TipoChamado
import br.ufrn.imd.atende.modelo.Usuario

class Listas {

	static List<Chamado> chamados = new ArrayList<Chamado>()
	static List<TipoChamado> tiposChamado =  new ArrayList<TipoChamado>()
	static List<SituacaoChamado> situacoes = new ArrayList<SituacaoChamado>()
	static List<MovimentacaoChamado> movimentacoes = new ArrayList<MovimentacaoChamado>()
	static List<Usuario> usuarios = new ArrayList<Usuario>()
	
	static findByLogin (login) {
		for(usuario in usuarios){
			if(usuario.login.equals(login))
				return usuario
		}
	}
	
	static findChamadosResponsavel(login) {
		List<Chamado> meusChamados = new ArrayList<Chamado>() 
		for(chamado in chamados){
			if(chamado.responsavel.login.equals(login)) {
				meusChamados.add(chamado)	
			}
		}
		return meusChamados
	}
	
	static findChamados() {
		chamados
	}
	
}
