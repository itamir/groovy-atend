package br.ufrn.imd.atende.main

import br.ufrn.imd.atende.dao.Listas
import br.ufrn.imd.atende.modelo.SituacaoChamado
import br.ufrn.imd.atende.modelo.TipoChamado
import br.ufrn.imd.atende.modelo.Usuario

/**
 * Classe para inicializa��es das vari�veis da aplica��o.
 * @author itamir
 *
 */
class Inicializacoes {

	/**
	 * Inicializa o que � necess�rio para o programa.
	 */
	static void start() {
		//usuario admin
		def usuario = new Usuario(login: "admin", senha: "admin", perfil: "gerente" ,nome: "Itamir", cpf: "1232123123")
		Listas.usuarios.add(usuario)
		//tipo chamado
		def suporteRedes = new TipoChamado(nome: "Redes", descricao: "Abertura de chamados para o setor de redes." )
		Listas.tiposChamado.add(suporteRedes)
		def suporteSistemas = new TipoChamado(nome: "Sistemas", descricao: "Abertura de chamados para o setor de sistemas." )
		Listas.tiposChamado.add(suporteSistemas)
		//situa��o situa��o chamado
		def atendido = new SituacaoChamado(descricao: "Atendido" )
		Listas.situacoes.add(atendido)
		def encaminhado = new SituacaoChamado(descricao: "Encaminhado" )
		Listas.situacoes.add(encaminhado)
		def naoResolvido = new SituacaoChamado(descricao: "N�o resolvido" )
		Listas.situacoes.add(naoResolvido)
	}
	
}
