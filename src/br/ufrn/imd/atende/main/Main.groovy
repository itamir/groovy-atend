package br.ufrn.imd.atende.main

import javax.swing.JOptionPane;
import javax.swing.JPasswordField;

import br.ufrn.imd.atende.dao.Listas;
import br.ufrn.imd.atende.modelo.Usuario

//iniciando o necess�rio para aplica��o
Inicializacoes.start()

def opcao = ""
while (!opcao.equals("0")) {
	//obtendo usu�rio
	def login = JOptionPane.showInputDialog("Digite seu login:")
	def usuario = Listas.findByLogin(login)
	//verificando se o usu�rio existe
	if(usuario != null) {
		def senha = JOptionPane.showInputDialog("Digite sua senha:")
		if(usuario.senha == senha) {
		//usu�rio logou, verificando perfil
			if(usuario.perfil.equals("gerente")) {
				def menuGerente = 
				"""
					Qual a op��o desejada?
					1 - Cadastrar Usu�rio
					2 - Alterar Usu�rio
					3 - Remover Usu�rio
					4 - Listar Chamados
					5 - Atribuir Chamados a Respons�vel 
				"""
				def opcaoGerente = JOptionPane.showInputDialog(menuGerente)
				
				switch(opcaoGerente) {
					case "4":
					def listagem = ""
					for(chamado in Listas.findChamados()) {
						listagem += chamado.assunto + " "
					}	
					break
				}
					
			} else if(usuario.perfil.equals("suporte")) {
				def menuSuporte =
				    """
						Qual a op��o desejada?
						1 - Listar meus chamados
						2 - Movimentar chamado
					"""
				def opcaoSuporte = JOptionPane.showInputDialog(menuSuporte)
			} else {
				def menuUsuario =
					"""
						Qual a op��o desejada?
						1 - Listar meus chamados
						2 - Movimentar chamado
					"""
				def opcaoUsuario = JOptionPane.showInputDialog(menuUsuario)
			}
		} else {
			opcao = JOptionPane.showInputDialog("Senha incorreta. Digite 0 para sair.")
		}
	} else {
		opcao = JOptionPane.showInputDialog("Usu�rio n�o encontrado. Digite 0 para sair.")
	}
} 